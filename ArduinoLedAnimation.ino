#include <Button.h>
#include "FastLED.h"
#include <Wire.h>
#include "RTClib.h"


FASTLED_USING_NAMESPACE

#define DATA_PIN    3
#define LED_TYPE    WS2811
#define COLOR_ORDER GRB
#define NUM_LEDS    3
CRGB leds[NUM_LEDS];

#define REDPIN   9
#define GREENPIN 10
#define BLUEPIN  11

#define RTC_INTERRUPT_PIN 3

const int ledPin =  LED_BUILTIN;
int ledState = LOW;

#define BUTTON_PIN A2       //Connect a tactile button switch (or something similar)
                           //from Arduino pin 2 to ground.
#define PULLUP true        //To keep things simple, we use the Arduino's internal pullup resistor.
#define INVERT true        //Since the pullup resistor will keep the pin high unless the
                           //switch is closed, this is negative logic, i.e. a high state
                           //means the button is NOT pressed. (Assuming a normally open switch.)
#define DEBOUNCE_MS 30     //A debounce time of 20 milliseconds usually works well for tactile button switches.
Button myBtn(BUTTON_PIN, PULLUP, INVERT, DEBOUNCE_MS);

//RTC node---------------------------------------------

RTC_DS1307 rtc;
volatile unsigned long totalMs = 0;
volatile unsigned long pulseCount = 0;
volatile float currentMs = 0;
volatile bool resetFlag;

unsigned long rtcMillis() {
  return totalMs + (int)currentMs;
}

void rtcNodeSetup() {
  pinMode(RTC_INTERRUPT_PIN, INPUT_PULLUP);
  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1);
  }
  rtc.writeSqwPinMode(SquareWave4kHz);
  if (! rtc.isrunning()) {
    Serial.println("RTC is NOT running!");
     rtc.adjust(DateTime(2017, 1, 21, 3, 0, 0));
  }
  attachInterrupt(digitalPinToInterrupt(RTC_INTERRUPT_PIN), rtcTick, FALLING);
}

void rtcTick(){
  if (resetFlag) {
    currentMs = pulseCount = totalMs = 0;
    resetFlag = false;
  }
  pulseCount++;
  currentMs += 1000.f / 4096;
  if (pulseCount == 4096) {
      currentMs = 0;
      pulseCount = 0;
      totalMs += 1000;
  }
}

void rtcResetClock(){
  resetFlag = true;
}

//-----------------------------------------------------


void setup() {
  Serial.begin(115200);
  FastLED.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
  pinMode(ledPin,OUTPUT);
  rtcNodeSetup();
}

void showLeds() {
  analogWrite(REDPIN,   leds[0].r);
  analogWrite(GREENPIN,   leds[1].r);
  analogWrite(BLUEPIN,   leds[2].r);
}

// List of patterns to cycle through.  Each is defined as a separate function below.
typedef void (*SimplePatternList[])();
SimplePatternList gPatterns = { rainbow, rainbowWithGlitter, confetti, sinelon, juggle, bpm };

uint8_t gCurrentPatternNumber = 0; // Index number of which pattern is current
uint8_t gHue = 0; // rotating "base color" used by many of the patterns

bool isAnimationStarted = false;

void loop()
{
  static bool ledState = false;
  myBtn.read();                    //Read the button
  if (myBtn.wasReleased()) {       //If the button was released, change the LED state
      ledState = !ledState;
      digitalWrite(ledPin, ledState);
      isAnimationStarted = !isAnimationStarted;
      if (isAnimationStarted) {
        rtcResetClock();
      }
  } 

  if (!isAnimationStarted)
    return;
  
  gPatterns[gCurrentPatternNumber]();
  showLeds();

  static unsigned long prevMillis = 0;
  unsigned long currentRtcMillis = rtcMillis();
  if (currentRtcMillis - prevMillis >= 1000) {
    prevMillis = currentRtcMillis;
    Serial.println(currentRtcMillis);
  }

  EVERY_N_MILLISECONDS( 20 ) { gHue++; } // slowly cycle the "base color" through the rainbow
  EVERY_N_SECONDS( 10 ) { nextPattern(); } // change patterns periodically

}

#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))

void nextPattern()
{
  // add one to the current pattern number, and wrap around at the end
  gCurrentPatternNumber = (gCurrentPatternNumber + 1) % ARRAY_SIZE( gPatterns);
}

void rainbow() 
{
  // FastLED's built-in rainbow generator
  fill_rainbow( leds, NUM_LEDS, gHue, 7);
}

void rainbowWithGlitter() 
{
  // built-in FastLED rainbow, plus some random sparkly glitter
  rainbow();
  addGlitter(80);
}

void addGlitter( fract8 chanceOfGlitter) 
{
  if( random8() < chanceOfGlitter) {
    leds[ random16(NUM_LEDS) ] += CRGB::White;
  }
}

void confetti() 
{
  // random colored speckles that blink in and fade smoothly
  fadeToBlackBy( leds, NUM_LEDS, 10);
  int pos = random16(NUM_LEDS);
  leds[pos] += CHSV( gHue + random8(64), 200, 255);
}

void sinelon()
{
  // a colored dot sweeping back and forth, with fading trails
  fadeToBlackBy( leds, NUM_LEDS, 20);
  int pos = beatsin16( 13, 0, NUM_LEDS-1 );
  leds[pos] += CHSV( gHue, 255, 192);
}

void bpm()
{
  // colored stripes pulsing at a defined Beats-Per-Minute (BPM)
  uint8_t BeatsPerMinute = 62;
  CRGBPalette16 palette = PartyColors_p;
  uint8_t beat = beatsin8( BeatsPerMinute, 64, 255);
  for( int i = 0; i < NUM_LEDS; i++) { //9948
    leds[i] = ColorFromPalette(palette, gHue+(i*2), beat-gHue+(i*10));
  }
}

void juggle() {
  // eight colored dots, weaving in and out of sync with each other
  fadeToBlackBy( leds, NUM_LEDS, 20);
  byte dothue = 0;
  for( int i = 0; i < 8; i++) {
    leds[beatsin16( i+7, 0, NUM_LEDS-1 )] |= CHSV(dothue, 200, 255);
    dothue += 32;
  }
}

